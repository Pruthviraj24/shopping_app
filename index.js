const express = require("express");
const bodyparser = require("body-parser")

const app = express();


app.use(function (req, res, next) {

  res.setHeader('Access-Control-Allow-Origin', '*');
  
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  res.setHeader('Access-Control-Allow-Credentials', true);

  next();
});



app.use(bodyparser.json());



// Routes

app.use('/',require('./routes/home'))
app.use("/",require("./routes/api"))



app.listen(3000, () => {
  console.log("Server is running on http://localhost:3000/");
});


























//Examples

// app.get("/productstable",async (req,response)=>{
//   const  sql = `SELECT * FROM products`

//   const data = await database.all(sql,[],(err,rows)=>{
//   if(err) return console.error(err.message)
//       else{
//           return rows
//       }
//   })

//   response.send(data)
// })


// app.get("/users",async (req,response)=>{
//   const  sql = `SELECT * FROM users`

//   const data = await database.all(sql,[],(err,rows)=>{
//   if(err) return console.error(err.message)
//       else{
//           return rows
//       }
//   })

//   response.send(data)
// })


// app.get("/orders",async (req,response)=>{
//   const  sql = `SELECT * FROM order_table`

//   const data = await database.all(sql,[],(err,rows)=>{
//   if(err) return console.error(err.message)
//       else{
//           return rows
//       }
//   })

//   response.send(data)
// })

// app.get("/userorder",async (req,response)=>{
//   const  sql = `SELECT * FROM user_order`

//   const data = await database.all(sql,[],(err,rows)=>{
//   if(err) return console.error(err.message)
//       else{
//           return rows
//       }
//   })

//   response.send(data)
// })






  // let sql; 

  // const db = new sqlite3.Database("./products.db", sqlite3.OPEN_READWRITE,(err)=>{
  //     if(err) return console.error(err.message);
  // })


// sql =`UPDATE products SET currencyId = "INR"`

  // sql = `CREATE TABLE order_table(
  //   id PRIMARY KEY,
  //   order_id,
  //   user_id,
  //   product_id,
  //   quantity,
  //   price,
  //   date_time
  // )`


// sql = `CREATE TABLE users(
//   id INT PRIMARY KEY,
//   email
// )`


// sql = `CREATE TABLE user_order(
//       id PRIMARY KEY,
//       user_id,
//       order_id
//     )`

// sql = `DROP TABLE order_table`

// sql = `CREATE TABLE products(
//         id PRIMARY KEY,
//         currencyFormat,
//         description,
//         price FLOAT,
//         style,
//         title,
//         productImage,
//         cartImage,
//         size)`

// sql = `DROP TABLE order_table`


// sql = `CREATE TABLE cart(
//   id PRIMARY KEY,
//   cartItem
// )`

// sql = `INSERT INTO users(
//   id,
//   email
// )
// VALUES(?,?)`


// db.run(sql)



// sql = `INSERT INTO products(
//     id,
//     currencyId,
//     description,
//     price,
//     style,
//     title,
//     productImage,
//     cartImage ,
//      size)
//     VALUES (?,?,?,?,?,?,?,?,?)`
  
//   const values = [42,"$","USD","4 MSL",3,"Wine","Wine Skul T-Shirt","10686354557628304_1.jpg","10686354557628304_2.jpg","S,XL,M"]
  // const values = [v4(),"rahul@mail.com"]
//   db.run(sql,[...values],(err)=>{
//     if(err) return console.error(err.message);
// })

// const values = [
//     [v4(),"INR","4 MSL",10.9,"Black with custom print","Cat Tee Black T-Shirt","100_1.jpg","100_2.jpg", "S,M"],
//     [v4(),"INR","4 MSL",29.45,"Front print and paisley print","Dark Thug Blue-Navy T-Shirt","101_1.jpg","101_2.jpg","L,M"],
//     [v4(),"INR","GPX Poly 1",9,"Front tie dye print","Sphynx Tie Dye Wine T-Shirt","10412368723880252_1.jpg","10412368723880252_2.jpg","S,XL,L"],
//     [v4(),"INR","4 MSL",3,"Wine","Wine Skul T-Shirt","10686354557628304_1.jpg","10686354557628304_2.jpg","S,XL,M"],
//     [v4(),"INR","14/15 s/nº",10.9,"Branco com listras pretas","Cat Tee Black T-Shirt","11033926921508488_1.jpg","11033926921508488_2.jpg","M,XL,L"],
//     [v4(),"INR","14/15 s/nº",10.9,"Preta com listras brancas","Sphynx Tie Dye Grey T-Shirt","11600983276356164_1.jpg","11600983276356164_2.jpg","XXL"],
//     [v4(),"INR","14/15 s/nº",10.9,"Branco com listras pretas","Danger Knife Grey","11854078013954528_1.jpg","11854078013954528_2.jpg","M,S"],
//     [v4(),"INR","2014 s/nº",14.9,"Preto com listras brancas","White DGK Script Tee","12064273040195392_1.jpg","12064273040195392_2.jpg","S,XL,L"],
//     [v4(),"INR","14/15 s/nº - Jogador",14.9,"Branco com listras pretas","Born On The Streets","18532669286405344_1.jpg","18532669286405344_2.jpg","S,M,L"],
//     [v4(),"INR","14/15 + Camiseta 1º Mundial",25.9,"Preto","Tso 3D Short Sleeve T-Shirt A","18644119330491310_1.jpg","18644119330491310_2.jpg","M,XL,L"],
//     [v4(),"INR","Goleiro 13/14",10.9,"Branco","Man Tie Dye Cinza Grey T-Shirt","27250082398145996_1.jpg","27250082398145996_2.jpg","XS,XL,L"],
//     [v4(),"INR","1977 Infantil",49.9,"Preto com listras brancas","Crazy Monkey Black T-Shirt","39876704341265610_1.jpg","39876704341265610_2.jpg","L"],
//     [v4(),"INR","Azul escuro",22.5,"Azul escuro","Tso 3D Black T-Shirt","51498472915966370_1.jpg","51498472915966370_2.jpg","S,XL"],
//     [v4(),"INR","4 MSL",18.7,"Black with custom print","Crazy Monkey Grey","5619496040738316_1.jpg","5619496040738316_2.jpg","XL,L"],
//     [v4(),"INR","4 MSL",134.9,"Black with custom print","Cat Tee Black T-Shirt","6090484789343891_1.jpg","6090484789343891_2.jpg","S"],
//     [v4(),"INR","4 MSL",49,"Black with custom print","On The Streets Black T-Shirt","100_1.jpg","100_2.jpg","M,XL,M"],
//     [v4(),"INR","4 MSL",10.9,"Black with custom print","Cat Tee Black T-Shirt","100_1.jpg","100_2.jpg","XL,M"],
//     [v4(),"INR","14/15 s/nº",10.9,"Branco com listras pretas","Cat Tee Black T-Shirt","11033926921508488_1.jpg","11033926921508488_2.jpg","M,XL,L"],
//     [v4(),"INR","4 MSL",10.9,"Black with custom print","Cat Tee Black T-Shirt","100_1.jpg","100_2.jpg","S,XL,L,M,XXL,XS"],
//     [v4(),"INR","4 MSL",10.9,"Black with custom print","Cat Tee Black T-Shirt","100_1.jpg","100_2.jpg","S,XL,L,M,XXL"],
//     [v4(),"INR","Azul escuro",22.5,"Azul escuro","Tso 3D Black T-Shirt","51498472915966370_1.jpg","51498472915966370_2.jpg","S,XL,L,M"],
//     [v4(),"INR","4 MSL",18.7,"Black with custom print","Crazy Monkey Grey","5619496040738316_1.jpg","5619496040738316_2.jpg","S,M,XXL,XS"],
//     [v4(),"INR","4 MSL",134.9,"Black with custom print","Cat Tee Black T-Shirt","6090484789343891_1.jpg","6090484789343891_2.jpg","L,M,XXL"],
//     [v4(),"INR","14/15 s/nº",10.9,"Branco com listras pretas","Cat Tee Black T-Shirt","11033926921508488_1.jpg","11033926921508488_2.jpg","S,XL"],
//     [v4(),"INR","14/15 s/nº",10.9,"Preta com listras brancas","Sphynx Tie Dye Grey T-Shirt","11600983276356164_1.jpg","11600983276356164_2.jpg","XS"],
//     [v4(),"INR","14/15 s/nº",10.9,"Branco com listras pretas","Danger Knife Grey","11854078013954528_1.jpg","11854078013954528_2.jpg","M"],
//     [v4(),"INR","2014 s/nº",14.9,"Preto com listras brancas","White DGK Script Tee","12064273040195392_1.jpg","12064273040195392_2.jpg","S,M,XXL"],
//     [v4(),"INR","14/15 + Camiseta 1º Mundial",25.9,"Preto","Tso 3D Short Sleeve T-Shirt A","18644119330491310_1.jpg","18644119330491310_2.jpg","S,L,M,XXL"],
//     [v4(),"INR","Goleiro 13/14",10.9,"Branco","Man Tie Dye Cinza Grey T-Shirt","27250082398145996_1.jpg","27250082398145996_2.jpg","S"],
//     [v4(),"INR","1977 Infantil",49.9,"Preto com listras brancas","Crazy Monkey Black T-Shirt","39876704341265610_1.jpg","39876704341265610_2.jpg","M,XXL,XS"],
//     [v4(),"INR","Azul escuro",22.5,"Azul escuro","Tso 3D Black T-Shirt","51498472915966370_1.jpg","51498472915966370_2.jpg","XS"],
//     [v4(),"INR","4 MSL",18.7,"Black with custom print","Crazy Monkey Grey","5619496040738316_1.jpg","5619496040738316_2.jpg","S,XL,L,M,XXL,XS"],
// ]


// for(const data of values){
//     db.run(sql,[...data],(err)=>{
//         if(err) return console.error(err.message);
//     })
// }






// module.exports = app;
