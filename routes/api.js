const database = require("../initDatabase")
const express = require("express");
const router = express.Router()


//Get request for Admin page

router.get("/cartdata", async (request,response)=>{
  const  sql = 
  `SELECT  
    order_table.id AS id,
    users.email AS user_email,
    order_table.quantity AS quantity,
    order_table.price AS price,
    products.productImage AS image_url,
    products.title As title
  FROM order_table 
  INNER JOIN products ON order_table.product_id =  products.id
  INNER JOIN users ON users.id = order_table.user_id`

    await database.all(sql,[],(err,rows)=>{
    if(err) return console.error(err.message)
        else{
          response.send(rows)
        }
    })
})



//Get ordered products based on user
router.get("/products/:username", async (request,response)=>{
  const user_email = request.params.username.substring(1)
  const sql = 
  `SELECT  
    order_table.id AS id,
    users.email AS user_email,
    order_table.quantity AS quantity,
    order_table.price AS price,
    products.productImage AS image_url
  FROM order_table 
  INNER JOIN products ON order_table.product_id =  products.id
  INNER JOIN users ON users.id = order_table.user_id
  WHERE users.email LIKE "%${user_email}%"`

  const data = await database.all(sql,[],(err,rows)=>{
    if(err) return console.error(err.message)
        else{
            return rows
        }
    })
  response.send(data)

})



  module.exports = router;