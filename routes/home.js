const database = require("../initDatabase")
const express = require("express");
const router = express.Router()
const format = require("date-fns/format");
const {v4} = require("uuid")


// Get API to fetch first 16 products

router.get("/", async (request,response)=>{
    const  sql = `SELECT * FROM products LIMIT 16`
  
      const data = await database.all(sql,[],(err,rows)=>{
      if(err) return console.error(err.message)
          else{
            response.send(rows)
          }
      }) 
  })


//Get resquest for 16 more products

router.get("/search", async (request,response)=>{
    const sql = `SELECT * FROM products LIMIT 16 OFFSET 16`

    const data = await database.all(sql,[],(err,rows)=>{
    if(err) return console.error(err.message)
        else{
          response.send(rows)

        }
    })

})



//Get resquest based on user search input

router.get("/search/:searchInput", async (request,response)=>{
  const searchInput = request.params.searchInput.substring(1)

  
  const sql = `SELECT * FROM products WHERE title LIKE "%${searchInput}%" LIMIT 16`
  const data = await database.all(sql,[],(err,rows)=>{
    if(err) return console.error(err.message)
        else{
          response.send(rows)
        }
    })
})


//Get request based on size 

router.get("/size/:sizes", async (request,response)=>{
  const size = request.params.sizes.substring(1)
  
  const sql = `SELECT * FROM products WHERE size LIKE "%${size}%"`
   const data = await database.all(sql,[],(err,rows)=>{
    if(err) return console.error(err.message)
        else{
          response.send(rows)
        }
    })

  })




//Post Request to save User and Orders data

router.post("/postdata",async (request ,response)=>{
    
    const cartData = JSON.stringify(request.body);
    const cartDetails = JSON.parse(cartData)
    //Checking if user already exist
  
    const sql = `SELECT id FROM users WHERE email LIKE "%${cartDetails.userEmail}%"`
    let user_data = await database.all(sql,[],async (err,rows)=>{
      if(err) return console.error(err.message)
          else{
            const sql_cart_data = `INSERT INTO order_table(
              id,
              order_id,
              user_id,
              product_id,
              quantity,
              price,
              date_time
              )VALUES(?,?,?,?,?,?,?)`
          
              const sql_order_product = `INSERT INTO user_order(
                id,
                user_id,
                order_id
              )VALUES(?,?,?)`
          
              const newDate = format(new Date(), "yyyy-MM-dd hh:mm:ss a");
          
              if(rows.length === 0){
                console.log(user_data);
                    const sql_user = `INSERT INTO users(
                          id,
                          email)
                          VALUES (?,?)`
                  const user_id = v4()
          
          
                  const updater = await database.run(sql_user,[user_id,cartDetails.userEmail],(err)=>{
                    if(err) return console.error(err.message);
                  })
              
          
          
                const order_user_updater = await database.run(sql_order_product,[v4(),user_id,cartDetails.orderId],(err)=>{
                  if(err) return console.error(err.message);
                })
          
          
                for(let productDetails of cartDetails.orderDetails){
                  const insert_cart_data = 
                  [
                  v4(),
                  cartDetails.orderId,
                  user_id,
                  productDetails.id,
                  productDetails.quantity,
                  productDetails.price,
                  newDate
                ]
                  const order_table_updater  = await database.run(sql_cart_data,insert_cart_data,(err)=>{
                      if(err) return console.error(err.message);
                  })
          
            }
          
          
              }else{
                
                    const order_user_updater = await database.run(sql_order_product,[v4(),rows[0].id,cartDetails.orderId],(err)=>{
                      if(err) return console.error(err.message);
                    })
              
              
                for(let productDetails of cartDetails.orderDetails){
                      const insert_cart_data = 
                      [
                        v4(),
                        cartDetails.orderId,
                        rows[0].id,
                        productDetails.id,
                        productDetails.quantity,
                        productDetails.price,
                        newDate
                      ]
                      const order_table_updater  = await database.run(sql_cart_data,insert_cart_data,(err)=>{
                          if(err) return console.error(err.message);
                      })
                }
              }
          
              return response.json({
                status:200,
                success:true,
              })  
          }
      })
  
      console.log(user_data);
  
    

  })
  



  module.exports = router;