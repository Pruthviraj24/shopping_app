const sqlite3 = require("sqlite3").verbose();
const path = require("path");





let database;

const initializeDBandServer = async () => {
    try {
      database = new sqlite3.Database(path.join(__dirname, "products.db"));
    } catch (error) {
      console.log(`DataBase error is ${error.message}`);
      process.exit(1);
    }
  };
  
  initializeDBandServer();

  module.exports = database 